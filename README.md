# ILLACK PORTFOLIO

<img title="DEGREE APPRENTICE HUB" alt="DEGREE APPRENTICE HUB" src="public/static/images/illack_portfolio.png">

<br><br>




## WHY I HAVE A PORTFOLIO?
This portfolio is my way of showcasing my ambition as a developer/person by building a website from scratch which links to all my social media/saas.

In building this app I learned React, How to host an app on a cloud server, and how to use services like Hostinger to redirect HTTP/HTTPS Requests made to my domain name [illack.co.uk](https://www.illack.co.uk)
