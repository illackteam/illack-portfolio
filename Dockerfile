# Use an official Node.js runtime as a parent image
FROM node:16

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Install serve just incase not in image
RUN npm install serve


# Copy the rest of the application code to the working directory
COPY . .

# Build the application (if you have a build step)
RUN npm run build

# Expose the port the app runs on
EXPOSE 3000

# Define the command to run the application (adjust if necessary)
CMD ["npx", "serve", "-s", "build", "-l", "3000"]